import 'package:prueba_fulltime/core/error/failure.dart';
import 'package:prueba_fulltime/core/resource/data_state.dart';
import 'package:prueba_fulltime/data/data_source/remote/pokemon_service.dart';
import 'package:prueba_fulltime/domain/repository/pokemon_repository.dart';

class PokemonRepositoryImpl implements PokemonRepository {
  PokemonRepositoryImpl({required this.service});
  final PokemonService service;

  @override
  Future<DataState> getAllPokemons() async {
    try {
      final result = await service.getAllPokemon();
      if (result != null) {
        return DataSuccess(result);
      } else {
        return DataFailure(const Failure(
            message: "Error! no se pudieron obtener datos de la api"));
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<DataState> getPokemonById(int id) async {
    try {
      final result = await service.getDetailedPokemon(id);
      if (result != null) {
        return DataSuccess(result);
      } else {
        return DataFailure(const Failure(
            message: "Error! no se pudieron obtener los detalles del pokemon"));
      }
    } catch (e) {
      rethrow;
    }
  }
}
