import 'package:dio/dio.dart';
import 'package:prueba_fulltime/core/constants/constants.dart';
import 'package:prueba_fulltime/core/utils/utils.dart';
import 'package:prueba_fulltime/data/model/pokemon_details_model.dart';
import 'package:prueba_fulltime/data/model/pokemon_model.dart';
import 'package:prueba_fulltime/domain/entity/pokemon.dart';

class PokemonService {
  PokemonService({required this.dio});
  final Dio dio;

  Future<List<PokemonEntity>> getAllPokemon(
      {int? limit = 10, int? offset = 10}) async {
    var result = <PokemonModel>[];

    try {
      final data = await readJson('assets/pokemon.json');
      result = data.map((e) => PokemonModel.fromJson(e)).toList();
    } catch (e) {
      rethrow;
    }
    return result;
  }

  Future<PokemonDetailModel> getDetailedPokemon(int id) async {
    try {
      final response = await dio.get("${Constants.baseUrl}/$id");
      return PokemonDetailModel.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }
}
