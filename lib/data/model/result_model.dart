import 'package:prueba_fulltime/domain/entity/result_entity.dart';

class ResultModel extends ResultEntity {
  ResultModel({
    required super.count,
    required super.next,
    super.previous,
    required super.data,
  });

  ResultModel.fromJson(Map<String, dynamic> json)
      : super(
            count: json["count"],
            data: json["data"],
            next: json['next'],
            previous: json['previous']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['count'] = count;
    data['next'] = next;
    data['previous'] = previous;
    data['data'] = data;
    return data;
  }
}
