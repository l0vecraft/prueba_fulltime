import 'package:prueba_fulltime/domain/entity/pokemon.dart';

class PokemonModel extends PokemonEntity {
  PokemonModel({
    super.name,
    super.id,
    super.imageurl,
    super.xdescription,
    super.ydescription,
    super.height,
    super.category,
    super.weight,
    super.typeofpokemon,
    super.weaknesses,
    super.evolutions,
    super.abilities,
    super.hp,
    super.attack,
    super.defense,
    super.specialAttack,
    super.specialDefense,
    super.speed,
    super.total,
    super.malePercentage,
    super.femalePercentage,
    super.genderless,
    super.cycles,
    super.eggGroups,
    super.evolvedfrom,
    super.reason,
    super.baseExp,
  });

  PokemonModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    id = json['id'];
    imageurl = json['imageurl'];
    xdescription = json['xdescription'];
    ydescription = json['ydescription'];
    height = json['height'];
    category = json['category'];
    weight = json['weight'];
    typeofpokemon = json['typeofpokemon'].cast<String>();
    weaknesses = json['weaknesses'].cast<String>();
    evolutions = json['evolutions'].cast<String>();
    abilities = json['abilities'].cast<String>();
    hp = json['hp'];
    attack = json['attack'];
    defense = json['defense'];
    specialAttack = json['special_attack'];
    specialDefense = json['special_defense'];
    speed = json['speed'];
    total = json['total'];
    malePercentage = json['male_percentage'];
    femalePercentage = json['female_percentage'];
    genderless = json['genderless'];
    cycles = json['cycles'];
    eggGroups = json['egg_groups'];
    evolvedfrom = json['evolvedfrom'];
    reason = json['reason'];
    baseExp = json['base_exp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['id'] = id;
    data['imageurl'] = imageurl;
    data['xdescription'] = xdescription;
    data['ydescription'] = ydescription;
    data['height'] = height;
    data['category'] = category;
    data['weight'] = weight;
    data['typeofpokemon'] = typeofpokemon;
    data['weaknesses'] = weaknesses;
    data['evolutions'] = evolutions;
    data['abilities'] = abilities;
    data['hp'] = hp;
    data['attack'] = attack;
    data['defense'] = defense;
    data['special_attack'] = specialAttack;
    data['special_defense'] = specialDefense;
    data['speed'] = speed;
    data['total'] = total;
    data['male_percentage'] = malePercentage;
    data['female_percentage'] = femalePercentage;
    data['genderless'] = genderless;
    data['cycles'] = cycles;
    data['egg_groups'] = eggGroups;
    data['evolvedfrom'] = evolvedfrom;
    data['reason'] = reason;
    data['base_exp'] = baseExp;
    return data;
  }
}
