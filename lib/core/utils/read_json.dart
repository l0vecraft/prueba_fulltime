part of 'utils.dart';

Future<List<dynamic>> readJson(String path) async {
  final response = await rootBundle.loadString(path);
  final List result = jsonDecode(response);
  return result;
}
