part of 'utils.dart';

Color buildCardColor(String type) {
  switch (type.toLowerCase()) {
    case 'fire':
      return Colors.red.shade400;
    case 'water':
      return Colors.blue.shade400;
    case 'grass':
      return Colors.green.shade400;
    case 'electric':
      return Colors.yellow.shade400;
    case 'ice':
      return Colors.cyan.shade400;
    case 'poison':
      return Colors.purple.shade400;
    case 'flying':
      return Colors.lime.shade400;
    case 'bug':
      return Colors.lightGreen.shade400;
    case 'rock':
      return Colors.grey.shade400;
    case 'ghost':
    case 'dark':
      return Colors.deepPurple.shade400;
    case 'fairy':
      return Colors.pink.shade400;
    case 'psychic':
      return Colors.pinkAccent.shade400;
    case 'steel':
    case 'dragon':
      return Colors.indigo.shade400;
    case 'normal':
    default:
      return Colors.orange.shade400;
  }
}
