import 'package:prueba_fulltime/core/error/failure.dart';

class DataState<T> {
  DataState({this.data, this.error});
  final T? data;
  final Failure? error;
}

class DataSuccess<T> extends DataState {
  DataSuccess(T data) : super(data: data);
}

class DataFailure<T> extends DataState {
  DataFailure(Failure error) : super(error: error);
}
