library prueba_fulltime.presentation.screen;

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_fulltime/core/utils/utils.dart';
import 'package:prueba_fulltime/domain/usecase/usecase.dart';

import 'package:prueba_fulltime/presentation/widget/widgets.dart';

part 'home_screen.dart';
part 'detail_screen.dart';
