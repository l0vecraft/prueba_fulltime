part of "screen.dart";

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  @override
  void initState() {
    super.initState();
    ref.read(pokemonProvider.notifier).getAllPokemons();
  }

  @override
  Widget build(BuildContext context) {
    var pokeFuture = ref.watch(pokemonFutureProvider);
    var pokeController = ref.read(pokemonProvider.notifier);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Pokedex',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: pokeFuture.when(
          data: (data) => GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 8.h,
                    crossAxisSpacing: 6.w),
                padding: EdgeInsets.only(top: 10.h, left: 3.w, right: 3.w),
                itemBuilder: (context, index) => Card(
                  color:
                      buildCardColor(data?[index].typeofpokemon?.first ?? ''),
                  child: InkWell(
                    onTap: () {
                      pokeController.getPokemonDetail(data?[index].id ?? '0');
                      Navigator.of(context).pushNamed('/details');
                    },
                    borderRadius: BorderRadius.circular(10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 20.h),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                "${data?[index].id}",
                                style: TextStyle(
                                    color: Colors.white.withOpacity(.6),
                                    fontSize: 20.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Text(
                            "${data?[index].name}",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Flexible(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: data?[index]
                                            .typeofpokemon
                                            ?.map(
                                                (tipo) => TypePill(tipo: tipo))
                                            .toList() ??
                                        [],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Image.network(
                                    data?[index].imageurl ?? '',
                                    cacheHeight: 100,
                                    cacheWidth: 100,
                                    loadingBuilder:
                                        (context, child, loadingProgress) {
                                      if (loadingProgress == null) {
                                        return child;
                                      }
                                      return Center(
                                        child: CircularProgressIndicator(
                                          value: loadingProgress
                                                      .expectedTotalBytes !=
                                                  null
                                              ? loadingProgress
                                                      .cumulativeBytesLoaded /
                                                  loadingProgress
                                                      .expectedTotalBytes!
                                              : null,
                                          color: Colors.red,
                                          backgroundColor: Colors.red.shade200,
                                        ),
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
          error: (error, stackTrace) => Text("Ocurrio un error $error"),
          loading: () => const CustomProgressIndicator()),
    );
  }
}
