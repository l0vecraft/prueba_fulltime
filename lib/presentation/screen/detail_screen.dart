part of "screen.dart";

class DetailScreen extends ConsumerStatefulWidget {
  const DetailScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _DetailScreenState();
}

class _DetailScreenState extends ConsumerState<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    var state = ref.watch(pokemonProvider);
    var radius = const Radius.circular(20);
    return (state.isLoading ?? false)
        ? Container(
            color: Colors.white,
            alignment: Alignment.center,
            child: const CircularProgressIndicator(
              backgroundColor: Colors.red,
            ),
          )
        : Scaffold(
            backgroundColor: buildCardColor(
                state.selectedPokemon?.typeofpokemon?.first ?? ''),
            appBar: AppBar(
              leading: const BackButton(
                color: Colors.white,
              ),
              centerTitle: true,
              title: Text(
                '${state.selectedPokemon?.name}',
                style: const TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
              backgroundColor: Colors.red,
            ),
            body: Stack(
              fit: StackFit.expand,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            state.selectedPokemon?.name ?? '',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 25.sp),
                          ),
                          Text(
                            state.selectedPokemon?.id ?? '',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.sp),
                          ),
                        ],
                      ),
                      Row(
                        children: state.selectedPokemon?.typeofpokemon
                                ?.map((tipo) => Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 4.w),
                                      child: TypePill(tipo: tipo),
                                    ))
                                .toList() ??
                            [],
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: radius, topRight: radius)),
                      shadowColor: Colors.black,
                      elevation: 5,
                      child: SlideInUp(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15.w, vertical: 15.h),
                          child: DetailBody(pokemon: state.selectedPokemon!),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 110.h,
                  left: 100.w,
                  child: FadeIn(
                    duration: const Duration(seconds: 2),
                    child: Image.network(
                      state.selectedPokemon?.imageurl ?? '',
                      scale: 2.5,
                    ),
                  ),
                ),
              ],
            ));
  }
}
