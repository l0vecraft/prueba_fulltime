part of 'widgets.dart';

class TypePill extends StatelessWidget {
  const TypePill({
    required this.tipo,
    super.key,
  });
  final String tipo;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.h),
      child: Container(
        width: 90.w,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 2.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white.withOpacity(.5),
        ),
        child: Text(
          tipo,
          style: TextStyle(
              fontSize: tipo.length > 6 ? 12.sp : 14.sp,
              color: Colors.white,
              shadows: const [
                BoxShadow(
                    color: Colors.black, blurRadius: 5, offset: Offset(0, 1))
              ]),
        ),
      ),
    );
  }
}
