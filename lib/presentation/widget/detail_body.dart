part of 'widgets.dart';

class DetailBody extends StatelessWidget {
  const DetailBody({
    super.key,
    required this.pokemon,
  });

  final PokemonEntity pokemon;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(pokemon.xdescription ?? 'There is not description'),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: SizedBox(
            child: Card(
              elevation: 10,
              shadowColor: Colors.grey.shade300,
              surfaceTintColor: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  VerticalTitleAndText(title: "Peso", text: pokemon.weight),
                  VerticalTitleAndText(title: "Altura", text: pokemon.height)
                ],
              ),
            ),
          ),
        ),
        Text(
          "Crianza",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.sp),
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 10.w),
              child: const Text("Porcentajes",
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold)),
            ),
            Row(
              children: [
                const Icon(Icons.male, color: Colors.blue),
                Text(pokemon.malePercentage ?? "")
              ],
            ),
            Row(
              children: [
                const Icon(Icons.female, color: Colors.pink),
                Text(pokemon.femalePercentage ?? "")
              ],
            )
          ],
        ),
        Row(
          children: [
            const Text("Grupo",
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold)),
            SizedBox(width: 10.w),
            Text(pokemon.eggGroups ?? '')
          ],
        ),
      ],
    );
  }
}
