library prueba_fulltime.presentation.widgets;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_fulltime/domain/entity/pokemon.dart';

part 'custom_progress_indicator.dart';
part 'type_pill.dart';
part 'vertical_title_and_text.dart';
part 'detail_body.dart';
