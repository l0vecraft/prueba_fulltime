part of 'widgets.dart';

class VerticalTitleAndText extends StatelessWidget {
  const VerticalTitleAndText({
    super.key,
    required this.title,
    required this.text,
  });

  final String? title;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title ?? '',
            style: TextStyle(
                color: Colors.grey.shade500, fontWeight: FontWeight.bold),
          ),
          Text(text ?? '')
        ],
      ),
    );
  }
}
