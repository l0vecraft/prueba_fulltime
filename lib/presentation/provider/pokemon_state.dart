part of "providers.dart";

class PokemonState {
  final List<PokemonEntity>? pokemonData;
  final bool? isLoading;
  final PokemonDetailModel? pokemonDetail;
  final PokemonEntity? selectedPokemon;
  final Failure? error;

  PokemonState(
      {this.pokemonData,
      this.isLoading,
      this.pokemonDetail,
      this.error,
      this.selectedPokemon});

  PokemonState copyWith({
    List<PokemonEntity>? pokemonData,
    bool? isLoading,
    PokemonDetailModel? pokemonDetail,
    PokemonEntity? selectedPokemon,
    Failure? error,
  }) =>
      PokemonState(
        pokemonData: pokemonData ?? this.pokemonData,
        isLoading: isLoading ?? this.isLoading,
        pokemonDetail: pokemonDetail ?? this.pokemonDetail,
        selectedPokemon: selectedPokemon ?? this.selectedPokemon,
        error: error ?? this.error,
      );
}
