library prueba_fulltime.presentation.provider;

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:prueba_fulltime/core/error/failure.dart';
import 'package:prueba_fulltime/core/resource/data_state.dart';
import 'package:prueba_fulltime/data/model/pokemon_details_model.dart';
import 'package:prueba_fulltime/domain/entity/pokemon.dart';

import 'package:prueba_fulltime/domain/usecase/usecase.dart';

part 'pokemon_controller.dart';
part "pokemon_state.dart";
