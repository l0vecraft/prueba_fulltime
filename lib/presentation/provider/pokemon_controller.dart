part of "providers.dart";

class PokemonController extends StateNotifier<PokemonState> {
  PokemonController({
    required this.getAllPokemons,
    required this.getDetailPokemons,
  }) : super(PokemonState(pokemonData: []));

  final GetAllPokemons getAllPokemons;
  final GetDetailPokemon getDetailPokemons;

  Future<List<PokemonEntity>> fetchPokemons() async {
    var pokemons = <PokemonEntity>[];
    try {
      var result = await getAllPokemons.call();
      if (result is DataSuccess) {
        pokemons = result.data;
        state = state.copyWith(pokemonData: pokemons);
      } else {
        state = state.copyWith(pokemonData: pokemons);
        print(result.error);
      }
    } catch (e) {
      print("Upps=> $e");
    }
    return pokemons;
  }

  Future<void> getPokemonDetail(String id) async {
    var parsedId = int.parse(id.replaceAll("#", ""));
    var pokeSelected =
        state.pokemonData?.where((pokemon) => pokemon.id == id).first;
    state = state.copyWith(isLoading: true);
    var result = await getDetailPokemons.call(params: parsedId);
    if (result is DataSuccess) {
      state = state.copyWith(
          isLoading: false,
          pokemonDetail: result.data,
          selectedPokemon: pokeSelected);
    } else {
      state = state.copyWith(isLoading: false, error: result.error);
    }
  }
}
