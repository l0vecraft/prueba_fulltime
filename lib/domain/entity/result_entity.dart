import 'package:prueba_fulltime/data/model/pokemon_details_model.dart';

class ResultEntity {
  int count;
  String next;
  String? previous;
  List<PokemonDetailModel> data;

  ResultEntity({
    required this.count,
    required this.next,
    required this.data,
    this.previous,
  });
}
