library prueba_fulltime.presentation.usecase;

import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_fulltime/core/resource/data_state.dart';
import 'package:prueba_fulltime/core/usecase/use_case.dart';
import 'package:prueba_fulltime/data/data_source/remote/pokemon_service.dart';
import 'package:prueba_fulltime/data/repository_implementation/pokemon_repository_imp.dart';
import 'package:prueba_fulltime/domain/entity/pokemon.dart';
import 'package:prueba_fulltime/domain/repository/pokemon_repository.dart';
import 'package:prueba_fulltime/presentation/provider/providers.dart';

part "get_all_pokemons.dart";
part 'get_detail_pokemon.dart';

//Dependecies
final _dioProvider = Provider<Dio>((ref) => Dio());

//Services
final _pokemonApiServiceProvider =
    Provider((ref) => PokemonService(dio: ref.read(_dioProvider)));

//Repositories
final _pokemonRepository = Provider<PokemonRepository>((ref) =>
    PokemonRepositoryImpl(service: ref.read(_pokemonApiServiceProvider)));

//UseCases
final getPokemonUseCaseProvider = Provider<GetAllPokemons>(
    (ref) => GetAllPokemons(pokemonRepository: ref.read(_pokemonRepository)));

final getPokemonsDetailsUseCaseProvider = Provider<GetDetailPokemon>((ref) {
  return GetDetailPokemon(repository: ref.read(_pokemonRepository));
});

final pokemonProvider = StateNotifierProvider<PokemonController, PokemonState>(
    (ref) => PokemonController(
        getAllPokemons: ref.read(getPokemonUseCaseProvider),
        getDetailPokemons: ref.read(getPokemonsDetailsUseCaseProvider)));

final pokemonFutureProvider = FutureProvider<List<PokemonEntity>?>(
    (ref) async => await ref.read(pokemonProvider.notifier).fetchPokemons());
