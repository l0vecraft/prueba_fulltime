part of "usecase.dart";

class GetDetailPokemon implements UseCase<DataState, int?> {
  GetDetailPokemon({required PokemonRepository repository})
      : _pokemonRepository = repository;

  final PokemonRepository _pokemonRepository;

  @override
  Future<DataState> call({int? params}) =>
      _pokemonRepository.getPokemonById(params ?? 0);
}
