part of "usecase.dart";

class GetAllPokemons implements UseCase<DataState, void> {
  GetAllPokemons({required PokemonRepository pokemonRepository})
      : _pokemonRepository = pokemonRepository;
  final PokemonRepository _pokemonRepository;

  @override
  Future<DataState> call({params}) => _pokemonRepository.getAllPokemons();
}
