import 'package:prueba_fulltime/core/resource/data_state.dart';

abstract class PokemonRepository {
  Future<DataState> getAllPokemons();
  Future<DataState> getPokemonById(int id);
}
