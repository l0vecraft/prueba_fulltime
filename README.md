# prueba_fulltime

aqui estaria la prueba tecnica presentada por fulltime.
la prueba fue realizada siguiendo **clean code**.

para ejecutar solo posicionarse en la carpeta inicial donde se encuentra el `main.dart` y en una consola ejecutar `flutter run` o el modo debug

### Nota
la api de pokeapi se empleo para realizar peticiones solamente, pero casi ningun dato se esta usando de ella ya que los datos que proporciona son algo incompletos para una buena demostracion, asi que por iniciativa propia y para que esta se vea mucho mejor se descargo un archivo **json** un poco mas organizado para mostrar imagenes y demas datos relevantes